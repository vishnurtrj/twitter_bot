const Express = require('express'); //Express NPM module to create API
const app = new Express();


const RandomQuoteGen = require('get-random-quote');    //To generate the random quote
const TwitterModel = require('./models/twitter.js');

app.get('/search/tweets', (req, res) => {
    const SEARCH_QUERY = req.query.searchQuery;

    return TwitterModel.findTweets(SEARCH_QUERY).then((result) => {
        const statuses = (result && result.statuses) || [];
        
        res.send(statuses);
    }).catch((error) => {
        res.send(error);
    });
});

app.post('/status/update', (req, res) => {    
    return RandomQuoteGen()
        .then((quote) => {
            const CLEANED_QUOTE = (quote && quote.text) || 'Empty Quote';

            return TwitterModel.postTweet(CLEANED_QUOTE).then((result) => {
                res.send(result);
            });
        })
        .catch((error) => {
            res.send(error);
        });
});

app.listen(3000);

console.log('App is running on 3000')