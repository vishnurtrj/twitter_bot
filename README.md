/*****************************************/

1. "npm install" to install all the dependencies.
2. set all the twitter keys in your environment variable by using the given names below.

    i) CONSUMER_KEY
    ii) SECRET_KEY
    iii) ACCESS_TOKEN_KEY
    iv) ACCESS_TOKEN_SECRET
    v) HOST_NAME

    To Run the application:
    CONSUMER_KEY="<CONSUMER_KEY>" SECRET_KEY="<SECRET_KEY>" ACCESS_TOKEN_KEY="<ACCESS_TOKEN_KEY>" ACCESS_TOKEN_SECRET="<ACCESS_TOKEN_KEY>" HOST_NAME="<HOST_NAME_WITH_PORT>" node app.js

    To run the test:
    CONSUMER_KEY="<CONSUMER_KEY>" SECRET_KEY="<SECRET_KEY>" ACCESS_TOKEN_KEY="<ACCESS_TOKEN_KEY>" ACCESS_TOKEN_SECRET="<ACCESS_TOKEN_KEY>" HOST_NAME="<HOST_NAME_WITH_PORT>" npm test

    NOTE: To run the endpoint test cases, the application should be UP and RUNNNING.

    Test operation will perform both endpoint and functional testing.