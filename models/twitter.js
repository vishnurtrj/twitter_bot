
const Twitter = require('twitter'); //Twitter API to interact with the twitter feature

const config = require('../config.js');
const Twitter_API = new Twitter(config);

module.exports = {
    findTweets: function (searchQuery) {
        return new Promise((resolve, reject) => {
            if (searchQuery) {
                const params = {
                    q: searchQuery,       //search query containing '#node.js'
                };
    
                resolve( Twitter_API.get('search/tweets', params) )
            } else {
                reject('Search Query must be valid')
            }
        });
    },
    
    postTweet: function (tweetMessage) {
        return new Promise((resolve, reject) => {
            if (tweetMessage) {
                resolve( Twitter_API.post('statuses/update', { status: tweetMessage }) )
            } else {
                reject('Tweet message must be valid')
            }
        });
    }
}