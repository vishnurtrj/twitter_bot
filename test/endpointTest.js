const Chai = require('chai');
const ChaiHttp = require('chai-http');
const should = Chai.should();

Chai.use(ChaiHttp);

const SERVER = process.env.HOST_NAME || 'localhost:3000'; //Ex: 'localhost:3000'

describe('/GET tweets by search query', () => {
    it('it should GET the search query related tweets', (done) => {
        Chai.request(SERVER).get('/search/tweets')
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .query({ searchQuery: 'node.js' })
            .end((err, res) => {
                if(!err){                
                    res.should.have.status(200);
                    res.body.should.be.a('array');                
                    done();
                } else {
                    done(err);
                }                
            });
    });
});

describe('/POST status update', () => {
    it('it should POST the tweet as status update', (done) => {
        Chai.request(SERVER)
            .post('/status/update')
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')            
            .end((err, res) => {
                if(!err){                
                    res.should.have.status(200);
                    res.body.should.be.a('object');                
                    done();
                } else {
                    done(err);
                }                
            });
    });
});



