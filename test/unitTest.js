const Chai = require('chai');
const ChaiHttp = require('chai-http');
const should = Chai.should();
const expect = Chai.expect;
const ChaiAsPromised = require('chai-as-promised');
Chai.use(ChaiAsPromised).should();

const TwitterModel = require('../models/twitter.js');

Chai.use(ChaiHttp);

describe('GET tweets by search query', () => {
    it('it should GET the search query related tweets', () => {
        const searchQuery = 'node.js';

        return TwitterModel.findTweets(searchQuery).then((result) => {            
            expect(result).to.have.property('statuses')
        })
    });

    it('it should GET error since search query is not passed', () => {
        return TwitterModel.findTweets().catch((error) => {
            expect(error).to.be.equal('Search Query must be valid')
        })
    });
});

describe('POST status udpate', () => {
    it('it should POST the tweet as status update', () => {
        const randomString = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

        return TwitterModel.postTweet(randomString).then((result) => {            
            expect(result).to.have.property('created_at')
        }) 
    });

    it('it should GET error since the tweetmessage is not passed', () => {
        return TwitterModel.postTweet().catch((error) => {
            expect(error).to.be.equal('Tweet message must be valid')
        }) 
    });
});